
<!-- README.md is generated from README.Rmd. Please edit that file -->

# PHMCompareR

<!-- badges: start -->
<!-- badges: end -->

The goal of PHMCompareR is to statistically assess models developed
using clinically driven selection rules for Population Health Management
segmentation and provide assurance that utilisation measures are
homogonous within segments and heterogeneous across them.

## Installation

You can install the development version of PHMCompareR like so:

``` r
devtools::install_gitlab(repo = "https://gitlab.com/leeds_ndl/phmcomparer")
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(PHMCompareR)

PHMCompareR::homogeneity_test(
  phm_df = phm_, 
  old = "old_segment", 
  new = "new_segment", 
  measures = c("hospital_activity", "gp_activity")
  )
#> $data
#> # A tibble: 10,000 x 8
#>    nhs_number old_segment   age  cost hospital_activity prescr…¹ gp_ac…² new_s…³
#>         <int> <chr>       <dbl> <dbl>             <dbl>    <dbl>   <dbl> <chr>  
#>  1          1 C              40 2050.                 6       18       2 G      
#>  2          2 A              23  425                  0        3       7 J      
#>  3          3 A              34  175                  0        1       3 F      
#>  4          4 C              40 1326.                 3        9       7 H      
#>  5          5 E              18    0                  0        0       0 F      
#>  6          6 E              73  275                  0        3       4 J      
#>  7          7 C              16  844.                 3        4       0 G      
#>  8          8 B              84   50                  0        0       1 F      
#>  9          9 D              18  200                  0        2       3 F      
#> 10         10 D              25  242.                 1        0       0 F      
#> # … with 9,990 more rows, and abbreviated variable names ¹​prescriptions,
#> #   ²​gp_activity, ³​new_segment
#> 
#> $old
#> [1] "old_segment"
#> 
#> $new
#> [1] "new_segment"
#> 
#> $measures
#> [1] "hospital_activity" "gp_activity"      
#> 
#> $statistic
#> [1] "moments::skewness" "moments::kurtosis" "dineq::theil.wtd" 
#> 
#> $summarised_data
#> # A tibble: 60 x 5
#>    segment measure            value statistic         model
#>    <chr>   <chr>              <dbl> <chr>             <chr>
#>  1 A       hospital_activity 0.797  moments::skewness old  
#>  2 A       gp_activity       0.0723 moments::skewness old  
#>  3 A       hospital_activity 2.54   moments::skewness old  
#>  4 A       gp_activity       1.79   moments::skewness old  
#>  5 A       hospital_activity 0.0989 moments::skewness old  
#>  6 A       gp_activity       0.137  moments::skewness old  
#>  7 B       hospital_activity 0.866  moments::skewness old  
#>  8 B       gp_activity       0.0168 moments::skewness old  
#>  9 B       hospital_activity 2.67   moments::skewness old  
#> 10 B       gp_activity       1.80   moments::skewness old  
#> # … with 50 more rows
#> 
#> $overall_change
#> # A tibble: 3 x 2
#>   statistic         mean_change
#>   <chr>                   <dbl>
#> 1 dineq::theil.wtd       -0.945
#> 2 moments::kurtosis       4.78 
#> 3 moments::skewness      -0.405
```

## Data structures

``` r
summary(phm_)
#>    nhs_number    old_segment             age              cost       
#>  Min.   :    1   Length:10000       Min.   :  0.00   Min.   :   0.0  
#>  1st Qu.: 2501   Class :character   1st Qu.: 19.00   1st Qu.: 200.0  
#>  Median : 5000   Mode  :character   Median : 30.00   Median : 475.0  
#>  Mean   : 5000                      Mean   : 35.22   Mean   : 924.2  
#>  3rd Qu.: 7500                      3rd Qu.: 50.00   3rd Qu.:1587.7  
#>  Max.   :10000                      Max.   :111.00   Max.   :4277.7  
#>  hospital_activity prescriptions    gp_activity    new_segment       
#>  Min.   : 0.000    Min.   : 0.00   Min.   :0.000   Length:10000      
#>  1st Qu.: 0.000    1st Qu.: 1.00   1st Qu.:1.000   Class :character  
#>  Median : 0.000    Median : 3.00   Median :3.000   Mode  :character  
#>  Mean   : 2.501    Mean   : 5.42   Mean   :3.266                     
#>  3rd Qu.: 5.000    3rd Qu.: 8.00   3rd Qu.:5.000                     
#>  Max.   :14.000    Max.   :37.00   Max.   :7.000
```

------------------------------------------------------------------------
