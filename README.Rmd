---
output: github_document
---

<!-- README.md is generated from README.Rmd. Please edit that file -->

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  fig.path = "man/figures/README-",
  out.width = "100%"
)
```

# PHMCompareR

<!-- badges: start -->
<!-- badges: end -->

The goal of PHMCompareR is to statistically assess models developed using clinically driven selection rules for Population Health Management segmentation and provide assurance that utilisation measures are homogonous within  segments and heterogeneous across them.

## Installation

You can install the development version of PHMCompareR like so:

```{r install, eval=FALSE}
devtools::install_gitlab(repo = "https://gitlab.com/leeds_ndl/phmcomparer")

```

## Example

This is a basic example which shows you how to solve a common problem:

```{r example}
library(PHMCompareR)

PHMCompareR::homogeneity_test(
  phm_df = phm_, 
  old = "old_segment", 
  new = "new_segment", 
  measures = c("hospital_activity", "gp_activity")
  )


```


## Data structures

```{r phm_}
summary(phm_)

```

***
