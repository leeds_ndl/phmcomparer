#' Outcomes of 100 patients by old and new treatments
#'
#' A dataset containing the genders and outcomes of two
#' treatment groups of 100 patients.
#'
#' @format A data frame with 100 rows and 3 variables:
#'  - *treatment*: treatment, old or new
#'  - *treatment*: gender, male or female
#'  - *outcome*: outcome, failure or success
"treatment"
#' Sample input for old PHM segment comparison to new
#'
#' A dataset containing the age, cost, activity, prescriptions and GP activity
#' of 10,000 patients and their old/new phm segment
#'
#' @format A data frame with 10000 rows and 8 variables:
#'  - *nhs_number*: dummy pseudo ID
#'  - *old_segment*: old segment the patients was allocated to
#'  - *new_segment*: new segment the patients was allocated to
#'  - *age*: age in years
#'  - *cost*: yearly cost in GBP
#'  - *hospital_activity*: yearly hospital activity
#'  - *prescriptions*: yearly number of prescriptions
#'  - *gp_activity*: yearly number of gp appointments
"phm_"
